package com.capitalone.ondemandatm.util;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.NoHttpResponseException;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.protocol.HTTP;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.net.ssl.SSLHandshakeException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by cl-99 on 8/6/2015.
 */
public abstract class ResponseResolver<T> implements Callback<T> {

    boolean showFailureDialog = true;
    Context ctx;

    public ResponseResolver(Context ctx) {
        this.ctx = ctx;
    }

    public ResponseResolver(Context ctx, boolean showFailureDialog) {
        this.ctx = ctx;
        this.showFailureDialog = showFailureDialog;
    }

    public abstract void onSuccess(T t, Response response);

    public abstract void onFailure(RestError error);

    @Override
    public void success(T t, Response response) {
        ProgressDialog.dismissProgressDialog();
        onSuccess(t, response);
    }

    @Override
    public void failure(RetrofitError error) {
        ProgressDialog.dismissProgressDialog();
        String errorMessage = "";
        RestError body = null;
        switch (error.getKind()) {
            case NETWORK:
                errorMessage = resolveNetworkError(error.getCause());
                break;

            case HTTP:
                if (error.getResponse() != null) {
                    body = new Gson().fromJson(error.getBody().toString(), RestError.class);
                    errorMessage = body.message;
                } else {
                    errorMessage = AppConstants.MSG_DEFAULT_ERROR;
                }
                break;

            default:
                errorMessage = AppConstants.MSG_DEFAULT_ERROR;
                break;
        }
        if (showFailureDialog || error.getKind() == RetrofitError.Kind.NETWORK) {
            CommonUtil.showDialog(ctx, errorMessage);
        }
        onFailure(body == null ? new RestError() : body);
    }

    private String resolveNetworkError(Throwable cause) {

        /*
         * Check whether the exception was raised due to
         * no internet connection
         */
        if (cause instanceof UnknownHostException)
            return AppConstants.NO_INTERNET_MESSAGE;

        /*
         *  When the the server fails to respond in
         *  given TIME_OUT_INTERVAL
         */
        if (cause instanceof SocketTimeoutException)
            return AppConstants.REMOTE_SERVER_FAILED_MESSAGE;

        /*
         * In some circumstances, usually when under
         * heavy load, the web server may be able to
         * receive requests but unable to process them.
         * This may cause the server to drop the connection
         * to the client without giving any response.
         */
        else if (cause instanceof NoHttpResponseException)
            return AppConstants.NO_HTTP_RESPONSE_MESSAGE;

        /*
         * This exception signals that HttpClient is unable
         * to establish a connection with the target server
         * or proxy server within the given period of time.
         */
        else if (cause instanceof ConnectTimeoutException)
            return AppConstants.CONNECTION_TIME_OUT_MESSAGE;
        /*
         * This exception signals error in completion of an
         * SSL protocol handshake on a given SSL connection
         *
         */
        else if (cause instanceof SSLHandshakeException)
            return AppConstants.SSL_HANDSHAKE_FAILED;
        /*
         * This exception signals error in completion of an
         * SSL protocol handshake on a given SSL connection
         *
         */
        else if (cause instanceof ConnectException)
            return AppConstants.CONNECTION_REFUSED;

        return AppConstants.UNEXPECTED_ERROR_OCCURRED;
    }
}
