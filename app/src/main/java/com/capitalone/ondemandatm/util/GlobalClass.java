package com.capitalone.ondemandatm.util;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.capitalone.ondemandatm.entities.User;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by cl-99 on 7/17/2015.
 */
public class GlobalClass extends Application {

    public WebServices webServices;
    public String deviceToken = "";
    public String accessToken = "";
    public User user;

    @Override
    public void onCreate() {
        super.onCreate();
//        FontsOverride.setDefaultFont(this, "MONOSPACE", "ProximaNova-Reg.otf");
        accessToken = CommonUtil.getAccessToken(this);
//        if (!accessToken.isEmpty()) {
//            accessToken = "bearer " + accessToken;
//        }
        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath(AppConstants.DEFAULT_FONT)
                        .build()
        );
    }

    public WebServices getWebServices(Activity ctx) {
        if (webServices == null) {
            webServices = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL)
                    .setEndpoint(AppConstants.SERVER_URL)
                    .setConverter(new StringConverter()).build().create(WebServices.class);
        }
        if (ctx != null) {
            ProgressDialog.showOn(ctx);
        }
        return webServices;
    }
}
