package com.capitalone.ondemandatm.util;

import retrofit.mime.TypedString;

/**
 * Created by cl-99 on 7/23/2015.
 */
public class TypedJsonString extends TypedString {
    public TypedJsonString(String body) {
        super(body);
    }

    @Override public String mimeType() {
        return "application/json";
    }
}