package com.capitalone.ondemandatm.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by cl-99 on 8/6/2015.
 */
public class ImageUtils {

    final static String PROJECT_NAME = "Capital One";
    final int CAMERA_CAPTURE = 1;
    final String FROM_CAMERA = "Take Photo";
    final String FROM_GALLERY = "Choose from Library";
    final String CANCEL = "Cancel";
    final String CAMERA_CAPTURE_NOT_SUPPORTED = "This device doesn't support the camera capture!";
    final int SELECT_FILE = 5;
    Uri capturedImageUri;
    Uri picUri;
    Uri outputPicUri;
    int orientation;
    Context ctx;

    public ImageUtils(Context ctx) {
        this.ctx = ctx;
    }

    public void chooseImage() {
        final CharSequence[] items = {FROM_CAMERA, FROM_GALLERY,
                CANCEL};

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(FROM_CAMERA)) {
                    try {
                        // use standard intent to capture an image
                        Intent captureIntent = new Intent(
                                MediaStore.ACTION_IMAGE_CAPTURE);
                        // we will handle the returned data in onActivityResult
                        //captureIntent.putExtra(MediaStore.EXTRA_OUTPUT,picUri);
                        File destination = new File(Environment.getExternalStorageDirectory(),
                                System.currentTimeMillis() + ".jpg");

                        capturedImageUri = Uri.fromFile(destination);
                        captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);
                        ((Activity) ctx).startActivityForResult(captureIntent, CAMERA_CAPTURE);
                    } catch (ActivityNotFoundException a) {
                        Toast toast = Toast.makeText(ctx, CAMERA_CAPTURE_NOT_SUPPORTED, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                } else if (items[item].equals(FROM_GALLERY)) {
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    ((Activity) ctx).startActivityForResult(i, SELECT_FILE);
                } else if (items[item].equals(CANCEL)) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_CAPTURE) {
                // get the Uri for the captured image
                if (capturedImageUri != null) {
                    picUri = capturedImageUri;
                } else {
                    picUri = data.getData();
                }
                outputPicUri = picUri;
                //getting orientation
                String capturedPicturePath = picUri.getPath();
                ExifInterface exif = null;

                try {
                    exif = new ExifInterface(capturedPicturePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);
                Crop.of(picUri, outputPicUri).asSquare().start((Activity) ctx);
            } else if (requestCode == SELECT_FILE) {
                picUri = data.getData();
                //getting orientation
                String capturedPicturePath = picUri.getPath();
                ExifInterface exif = null;

                try {
                    exif = new ExifInterface(capturedPicturePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);
                outputPicUri = picUri;
                Crop.of(picUri, outputPicUri).asSquare().start((Activity) ctx);
            } else if (requestCode == Crop.REQUEST_CROP) {
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(ctx.getContentResolver(), outputPicUri);
                    ((ImageLoadingListener) ctx).onImageLoad(pictureCrop(orientation, bitmap));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static Bitmap getRoundedImage(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    public static Bitmap pictureCrop(int orientation, Bitmap bitmap) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = getTempImageFile();

        FileOutputStream fo;
        try {
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String picturePath = destination.getAbsolutePath();
        Bitmap temp = Bitmap.createBitmap(BitmapFactory.decodeFile(picturePath));
        Matrix matrix = new Matrix();

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            matrix.postRotate(90);
            temp = Bitmap.createBitmap(temp, 0, 0, temp.getWidth(), temp.getHeight(), matrix, true);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            matrix.postRotate(180);
            temp = Bitmap.createBitmap(temp, 0, 0, temp.getWidth(), temp.getHeight(), matrix, true);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            matrix.postRotate(270);
            temp = Bitmap.createBitmap(temp, 0, 0, temp.getWidth(), temp.getHeight(), matrix, true);
        }
        temp = getRoundedImage(temp);
        return temp;
    }

    public static File getTempImageFile() {
        return getTempImageFile(false);
    }

    public static File getTempImageFile(boolean forUpload) {
        File file = new File(
                getDirectory(),
                "userImage.jpg");

        if (!forUpload && !file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }


    private static File getDirectory() {
        File sdCard = Environment.getExternalStorageDirectory();
        File directory = new File(sdCard.getAbsolutePath() + "/" + PROJECT_NAME);

        // create directory if not exist
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        return directory;
    }

    public interface ImageLoadingListener {
        public void onImageLoad(Bitmap bitmap);
    }
}
