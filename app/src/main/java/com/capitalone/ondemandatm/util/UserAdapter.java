package com.capitalone.ondemandatm.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.capitalone.ondemandatm.R;
import com.capitalone.ondemandatm.entities.User;

import java.util.List;

/**
 * Created by cl-99 on 7/17/2015.
 */
public class UserAdapter extends BaseAdapter {

    public List<User> data;
    LayoutInflater inflater;

    public UserAdapter(Context ctx, List<User> data) {
        this.data = data;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.txt);
        tv.setText(data.get(position).toString());
        return convertView;
    }
}
