package com.capitalone.ondemandatm.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextPaint;
import android.util.Log;

import com.capitalone.ondemandatm.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;

import javax.security.auth.callback.Callback;

/**
 * Created by cl-99 on 7/21/2015.
 */
public class CommonUtil implements AppConstants {

    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static int getIdFromName(String variableName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(variableName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static void showDialog(Context ctx, String message) {
        showDialog(ctx, "Error", message, null, null, false);
    }

    public static void showDialog(Context ctx, String title, String message, boolean showCancel) {
        showDialog(ctx, title, message, null, null, showCancel);
    }

    public static void showDialog(Context ctx, String title, String message,
                                  OnClickListener positive, OnClickListener negative, boolean showCancel) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(ctx);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", positive == null ? new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        } : positive);
        if (showCancel) {
            builder.setNegativeButton("Cancel", negative == null ? new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            } : negative);
        }
        builder.show();
    }


    public static void applyCustomFont(Context context, TextInputLayout textInputLayout) {
        final Typeface tf = Typeface.createFromAsset(context.getAssets(), AppConstants.DEFAULT_FONT);
        textInputLayout.getEditText().setTypeface(tf);
        try {
            // Retrieve the CollapsingTextHelper Field
            final Field cthf = textInputLayout.getClass().getDeclaredField("mCollapsingTextHelper");
            cthf.setAccessible(true);

            // Retrieve an instance of CollapsingTextHelper and its TextPaint
            final Object cth = cthf.get(textInputLayout);
            final Field tpf = cth.getClass().getDeclaredField("mTextPaint");
            tpf.setAccessible(true);

            // Apply your Typeface to the CollapsingTextHelper TextPaint
            ((TextPaint) tpf.get(cth)).setTypeface(tf);
//            ((TextPaint) tpf.get(cth)).setColor(context.getResources().getColor(R.color.background_floating_material_dark));
        } catch (Exception ignored) {
        }
    }

    //Device Token
    public static void registerGCM(Context context) {
        try {
            // Check device for Play Services APK. If check succeeds, proceed with GCM registration.
            if (checkPlayServices(context)) {
//                gcm = GoogleCloudMessaging.getInstance(this);
                GlobalClass global = ((GlobalClass) context.getApplicationContext());
                global.deviceToken = getRegistrationId(context);

                if (global.deviceToken.isEmpty()) {
                    registerInBackground(context);
                }
            } else {
                Log.i("TAG", "No valid Google Play Services APK found.");
            }

        } catch (Exception e) {
            Log.e("exception GCM", e.toString() + " " + e.toString());
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private static boolean checkPlayServices(Context ctx) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(ctx);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) ctx,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("TAG", "This device is not supported.");
                ((Activity) ctx).finish();
            }
            return false;
        }
        return true;
    }

    private static void registerInBackground(final Context ctx) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                // String msg = "";
                GoogleCloudMessaging gcm;
                try {
                    gcm = GoogleCloudMessaging.getInstance(ctx);
                    GlobalClass global = (GlobalClass) ctx.getApplicationContext();
                    global.deviceToken = gcm.register(SENDER_ID);
                    // msg = "Device registered, registration ID=" + Utils.deviceToken;
                    ((Activity) ctx).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            Toast.makeText(Login.this,Data.regid,Toast.LENGTH_LONG).show();
                        }
                    });

                    // For this demo: we don't need to send it because the device will send
                    // upstream messages to a server that echo back the message using the
                    // 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(ctx, global.deviceToken);
                } catch (IOException ex) {
                    // msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return "";

            }

            @Override
            protected void onPostExecute(String msg) {
                //mDisplay.append(msg + "\n");
            }
        }.execute(null, null, null);
    }

    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    private static void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGcmPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.commit();
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private static SharedPreferences getGcmPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
    }

    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    private static String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i("TAG", "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        return registrationId;
    }

    public static PackageInfo getVersionInfo(Context ctx) {
        try {
            return ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getAccessToken(Context context) {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(context);
        return pref.getString("accessToken", "");
    }

    public static void saveAccessToken(String token, Context context) {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("accessToken", token);
        editor.commit();
    }






}
