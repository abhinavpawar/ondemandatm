package com.capitalone.ondemandatm.util;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.PartMap;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by cl-99 on 7/17/2015.
 */
public interface WebServices {

    @GET("/api/customer/getVerificationCode")
    void getVerificationCode(@Query("phoneNo") String phoneNo, Callback<String> cb);

    @FormUrlEncoded
    @POST("/api/customer/{type}")
    void loginOrRegisterUser(@Path("type") String type, @FieldMap Map<String, Object> body, Callback<String> cb);

    @GET("/api/booking/getCurrentBookingStatusForCustomer")
    void getBookingStatus(@Header("authorization") String accessToken, Callback<String> cb);

    @Multipart
    @PUT("/api/customer/updateProfile")
    void updateProfile(@Header("authorization") String accessToken, @PartMap Map<String, Object> body,
                       Callback<String> cb);

    @FormUrlEncoded
    @PUT("/api/customer/verifyNewNumber")
    void updateNumber(@Header("authorization") String accessToken, @FieldMap Map<String, Object> body,Callback<String> cb);

    @FormUrlEncoded
    @POST("/api/booking/sendFeedback")
    void sendFeedback(@FieldMap Map<String, Object> body,@Header("authorization") String accessToken, Callback<String> cb);

}
