package com.capitalone.ondemandatm.util;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cl-99 on 8/5/2015.
 */
public class RestError {
    public int statusCode;
    public String error;
    public String message;
}