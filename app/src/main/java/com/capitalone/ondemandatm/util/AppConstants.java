package com.capitalone.ondemandatm.util;

/**
 * Created by cl-99 on 7/22/2015.
 */
public interface AppConstants {

    String SERVER_URL = "http://capitalone.clicklabs.in:8000";

    String DEFAULT_FONT = "fonts/ProximaNova-Light.ttf";

    String SENDER_ID = "856250932622";

    String MSG_FIELD_MISSING = "Please input all fields";

    String MSG_DEFAULT_ERROR = "An error occurred.Please try again";

    String MSG_FLUSH_PREVIOUS = "You are already logged in some other device. Proceed to recreate session ?";

    String NO_INTERNET_MESSAGE = "Please check internet connectivity status";
    String REMOTE_SERVER_FAILED_MESSAGE = "Remote server failed to respond";
    String CONNECTION_TIME_OUT_MESSAGE = "Connection timed out";
    String NO_HTTP_RESPONSE_MESSAGE = "Remote server could not respond";
    String PARSING_ERROR_MESSAGE = "An error was procured while parsing";
    String RUNTIME_ERROR_MESSAGE = "An unexpected error occurred";
    String UNEXPECTED_ERROR_OCCURRED = "Internet connectivity issue";
    String SSL_HANDSHAKE_FAILED = "SSL handshake failed";
    String CONNECTION_REFUSED = "Connection was refused from server";
}

