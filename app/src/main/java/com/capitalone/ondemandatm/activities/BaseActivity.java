package com.capitalone.ondemandatm.activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.view.Window;
import android.view.WindowManager;

import com.capitalone.ondemandatm.util.AppConstants;
import com.capitalone.ondemandatm.util.GlobalClass;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by cl-99 on 7/23/2015.
 */
public class BaseActivity extends FragmentActivity implements AppConstants{

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void changeStatusBarColor(int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    public GlobalClass globalData() {
        return (GlobalClass) getApplication();
    }
}
