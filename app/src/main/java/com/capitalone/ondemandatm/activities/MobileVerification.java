package com.capitalone.ondemandatm.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.capitalone.ondemandatm.R;
import com.capitalone.ondemandatm.entities.User;
import com.capitalone.ondemandatm.util.CircleTransform;
import com.capitalone.ondemandatm.util.CommonUtil;
import com.capitalone.ondemandatm.util.ImageUtils;
import com.capitalone.ondemandatm.util.ResponseResolver;
import com.capitalone.ondemandatm.util.RestError;
import com.google.gson.Gson;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MobileVerification extends BaseActivity implements ImageUtils.ImageLoadingListener {

    EditText[] digits;
    Button verifyNumber;
    MessageBroadcast receiver = new MessageBroadcast();
    private static final int DIGIT_COUNT = 4;
    boolean isRegistered;
    TextView verifyPrimary;
    TextView verifySecondary;
    ImageView profilePic;
    ImageUtils imageUtil;
    boolean profileVerify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileVerify = getIntent().hasExtra("profile_verify");
        setContentView(profileVerify ? R.layout.activity_verify_phone_blue :
                R.layout.activity_verify_phone);
        profilePic = (ImageView) findViewById(R.id.imageView);
        verifyPrimary = (TextView) findViewById(R.id.verifyPrimaryText);
        verifySecondary = (TextView) findViewById(R.id.verifySecondaryText);

        if (globalData().user != null && globalData().user.profilePicURL != null) {
            if (globalData().user.profilePicURL.original != null && !globalData().user.profilePicURL.original.isEmpty()) {
                Picasso.with(this)
                        .load(globalData().user.profilePicURL.original)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .error(profileVerify ? R.drawable.avatar_icon_white : R.drawable.avatar_icon)
                        .placeholder(profileVerify ? R.drawable.avatar_icon_white : R.drawable.avatar_icon)
                        .transform(new CircleTransform()).fit()
                        .into(profilePic);
            }
        }

        digits = new EditText[DIGIT_COUNT];
        for (int i = 1; i <= DIGIT_COUNT; i++) {
            digits[i - 1] = (EditText) findViewById(CommonUtil.getIdFromName("digit" + i, R.id.class));
        }
        verifyNumber = (Button) findViewById(R.id.verifyNumber);
        isRegistered = getIntent().getBooleanExtra("isRegistered", false);
        registerReceiver(receiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.verifyNumber:
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < DIGIT_COUNT; i++) {
                    builder.append(digits[i].getText().toString());
                }
                if (builder.length() != DIGIT_COUNT) {
                    CommonUtil.showDialog(this, "Please enter a valid OTP.");
                } else {

                    if (profileVerify) {
                        updateNumber(builder.toString());
                    } else {
                        Map<String, Object> params = new HashMap<>();
                        if (!isRegistered) {
                            params.put("name", getIntent().getStringExtra("userName"));
                        } else {
                            params.put("flushPreviousSessions", false);
                        }
                        params.put("phoneNo", getIntent().getStringExtra("phoneNum"));
                        params.put("verificationCode", builder.toString());
                        params.put("deviceType", "ANDROID");
                        params.put("deviceToken", globalData().deviceToken);
                        //For Emulator
                        if (globalData().deviceToken.isEmpty()) {
                            globalData().deviceToken = "default";
                        }
                        params.put("appVersion", Integer.toString(CommonUtil.getVersionInfo(this).versionCode));
                        loginRegister(params);
                    }
                }
                break;

            case R.id.imageView:
                imageUtil = new ImageUtils(this);
                imageUtil.chooseImage();
                break;

            case R.id.close:
                finish();
                break;
        }
    }

    private void updateNumber(String code) {
        Map<String, Object> params = new HashMap<>();
        params.put("verificationCode", code);
        params.put("phoneNo", getIntent().getStringExtra("phoneNo"));
        globalData().getWebServices(this).updateNumber(globalData().accessToken, params,
                new ResponseResolver<String>(this) {
                    @Override
                    public void onSuccess(String s, Response response) {
                        try {
                            JSONObject obj = new JSONObject(s);
                            obj = obj.getJSONObject("data");
                            globalData().accessToken = obj.getString("accessToken");
                            globalData().accessToken = "bearer " + globalData().accessToken;
                            CommonUtil.saveAccessToken(globalData().accessToken, MobileVerification.this);
                            CommonUtil.showDialog(MobileVerification.this, "Success", "Phone number verified.", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }, null, false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(RestError error) {
//                        CommonUtil.showDialog(MobileVerification.this, "Success", "Phone number verified.", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                finish();
//                            }
//                        }, null, false);
                    }
                });
    }

    private void loginRegister(final Map<String, Object> params) {
        globalData().getWebServices(this).loginOrRegisterUser(isRegistered ? "login" : "register",
                params, new ResponseResolver<String>(this, false) {
                    @Override
                    public void onSuccess(String s, Response response) {
                        try {
                            JSONObject obj = new JSONObject(s);
                            obj = obj.getJSONObject("data");
                            globalData().accessToken = obj.getString("accessToken");
                            globalData().accessToken = "bearer " + globalData().accessToken;
                            CommonUtil.saveAccessToken(globalData().accessToken, MobileVerification.this);
                            globalData().user =
                                    new Gson().fromJson(obj.getJSONObject("userDetails").toString(), User.class);
                            Intent intent = new Intent(MobileVerification.this, UserProfile.class);
                            intent.putExtra("firstLogin", true);
                            startActivity(intent);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            CommonUtil.showDialog(MobileVerification.this, MSG_DEFAULT_ERROR);
                        }
                    }

                    @Override
                    public void onFailure(RestError error) {
                        String message = error.message;
                        if (message.contains("flush")) {
                            //Prompt to clear previous sessions
                            CommonUtil.showDialog(MobileVerification.this, "Error", MSG_FLUSH_PREVIOUS,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            params.put("flushPreviousSessions", true);
                                            loginRegister(params);
                                        }
                                    }, null, true);
                        } else if (message.contains("Verification Code")) {
                            verifyPrimary.setText("Oops! Number incorrect");
                            verifyPrimary.setTextColor(getResources().getColor(R.color.error_color));
                            verifySecondary.setText("Please try again.");
                        } else {
                            CommonUtil.showDialog(MobileVerification.this, message);
                        }
                    }

                });
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
        imageUtil = null;
    }

    @Override
    public void onImageLoad(Bitmap bitmap) {
        profilePic.setImageBitmap(bitmap);
    }

    class MessageBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null && bundle.containsKey("pdus")) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdus[0]);
                String senderNumber = sms.getOriginatingAddress();
                String message = sms.getMessageBody();
                updateOTP(message);
            }
        }
    }

    public void updateOTP(String message) {

        String mOrgName = "Capital One";
        Matcher mOrgPattern = Pattern.compile(mOrgName).matcher(message);

        if (mOrgPattern.find()) {
            String pattern = "\\d{4}+";
            Matcher m = Pattern.compile(pattern).matcher(message);
            if (m.find()) {
                String code = m.group().toString();
                for (int i = 0; i < code.length(); i++) {
                    digits[i].setText(code.charAt(i) + "");
                }
            } else {
                Log.d("OTP", "No Integer Parsed");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imageUtil.onActivityResult(requestCode, resultCode, data);
    }
}
