package com.capitalone.ondemandatm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.capitalone.ondemandatm.R;
import com.capitalone.ondemandatm.util.AppConstants;
import com.capitalone.ondemandatm.util.CommonUtil;
import com.capitalone.ondemandatm.util.GlobalClass;
import com.capitalone.ondemandatm.util.PhoneUtil;
import com.capitalone.ondemandatm.util.ResponseResolver;
import com.capitalone.ondemandatm.util.RestError;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class IntroActivity extends BaseActivity {

    EditText phoneNumber;
    EditText name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        name = (EditText) findViewById(R.id.name);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        CommonUtil.applyCustomFont(this, (TextInputLayout) findViewById(R.id.nameInputLayout));
        CommonUtil.applyCustomFont(this, (TextInputLayout) findViewById(R.id.phoneInputLayout));
        PhoneUtil.getInstance().setValidationFilter(PhoneUtil.TextType.PhoneNumber, phoneNumber);
        phoneNumber.setText("7696692057");
        name.setText("Amrit");
        CommonUtil.registerGCM(this);
    }

    public void onClick(View view) {
        String phoneNum = phoneNumber.getText().toString();
        if (name.getText().toString().isEmpty() || phoneNumber.getText().toString().isEmpty()) {
            CommonUtil.showDialog(this, MSG_FIELD_MISSING);
            return;
        }

        final String userName = name.getText().toString();
        final String phoneNo = phoneNum.replaceAll("[^0-9]", "");

        globalData().getWebServices(this).getVerificationCode(phoneNo, new ResponseResolver<String>(this) {
            @Override
            public void onSuccess(String s, Response response) {
                try {
                    JSONObject obj = new JSONObject(s);
                    if (obj.has("errorMessage")) {
                        CommonUtil.showDialog(IntroActivity.this, obj.getString("errorMessage"));
                    } else {
                        Intent intent = new Intent(IntroActivity.this, MobileVerification.class);
                        intent.putExtra("userName", userName);
                        intent.putExtra("phoneNum", phoneNo);
                        intent.putExtra("isRegistered", obj.getJSONObject("data").getBoolean("isRegistered"));
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    CommonUtil.showDialog(IntroActivity.this, "");
                }
            }

            @Override
            public void onFailure(RestError error) {

            }
        });
    }
}
