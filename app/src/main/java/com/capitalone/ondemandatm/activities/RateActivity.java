package com.capitalone.ondemandatm.activities;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.capitalone.ondemandatm.R;
import com.capitalone.ondemandatm.util.ResponseResolver;
import com.capitalone.ondemandatm.util.RestError;

import java.util.HashMap;
import java.util.Map;

import retrofit.client.Response;

/**
 * Created by Abhinav on 8/12/15.
 */
public class RateActivity extends BaseActivity {
    int finalRating;
    ImageView one;
    ImageView two;
    ImageView three;
    ImageView four;
    ImageView five;
    int bookingId = 293;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        one = (ImageView) findViewById(R.id.firstStar);
        two = (ImageView) findViewById(R.id.secondStar);
        three = (ImageView) findViewById(R.id.thirdStar);
        four = (ImageView) findViewById(R.id.fourthStar);
        five = (ImageView) findViewById(R.id.fifthStar);

        View.OnClickListener ratingClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = v.getId();
                switch (id) {
                    case R.id.firstStar: {
                        one.setImageResource(R.drawable.icon_filled_star);
                        two.setImageResource(R.drawable.icon_unfilled_star);
                        three.setImageResource(R.drawable.icon_unfilled_star);
                        four.setImageResource(R.drawable.icon_unfilled_star);
                        five.setImageResource(R.drawable.icon_unfilled_star);
                        finalRating = 1;

                        break;
                    }
                    case R.id.secondStar: {
                        one.setImageResource(R.drawable.icon_filled_star);
                        two.setImageResource(R.drawable.icon_filled_star);
                        three.setImageResource(R.drawable.icon_unfilled_star);
                        four.setImageResource(R.drawable.icon_unfilled_star);
                        five.setImageResource(R.drawable.icon_unfilled_star);
                        finalRating = 2;
                        break;
                    }
                    case R.id.thirdStar: {
                        one.setImageResource(R.drawable.icon_filled_star);
                        two.setImageResource(R.drawable.icon_filled_star);
                        three.setImageResource(R.drawable.icon_filled_star);
                        four.setImageResource(R.drawable.icon_unfilled_star);
                        five.setImageResource(R.drawable.icon_unfilled_star);
                        finalRating = 3;
                        break;
                    }
                    case R.id.fourthStar: {
                        one.setImageResource(R.drawable.icon_filled_star);
                        two.setImageResource(R.drawable.icon_filled_star);
                        three.setImageResource(R.drawable.icon_filled_star);
                        four.setImageResource(R.drawable.icon_filled_star);
                        five.setImageResource(R.drawable.icon_unfilled_star);

                        finalRating = 4;
                        break;
                    }
                    case R.id.fifthStar: {
                        one.setImageResource(R.drawable.icon_filled_star);
                        two.setImageResource(R.drawable.icon_filled_star);
                        three.setImageResource(R.drawable.icon_filled_star);
                        four.setImageResource(R.drawable.icon_filled_star);
                        five.setImageResource(R.drawable.icon_filled_star);
                        finalRating = 5;
                        break;

                    }
                }
                //Toast.makeText(RateActivity.this, Integer.toString(finalRating), Toast.LENGTH_SHORT).show();
            }
        };
        View.OnTouchListener ratingTouch = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int id = v.getId();

                switch (id) {
                    case R.id.firstStar: {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            one.setImageResource(R.drawable.icon_filled_star);
                            two.setImageResource(R.drawable.icon_unfilled_star);
                            three.setImageResource(R.drawable.icon_unfilled_star);
                            four.setImageResource(R.drawable.icon_unfilled_star);
                            five.setImageResource(R.drawable.icon_unfilled_star);
                            finalRating = 1;
                        }
                        break;
                    }
                    case R.id.secondStar: {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            one.setImageResource(R.drawable.icon_filled_star);
                            two.setImageResource(R.drawable.icon_filled_star);
                            three.setImageResource(R.drawable.icon_unfilled_star);
                            four.setImageResource(R.drawable.icon_unfilled_star);
                            five.setImageResource(R.drawable.icon_unfilled_star);
                            finalRating = 2;
                        }
                        break;
                    }
                    case R.id.thirdStar: {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            one.setImageResource(R.drawable.icon_filled_star);
                            two.setImageResource(R.drawable.icon_filled_star);
                            three.setImageResource(R.drawable.icon_filled_star);
                            four.setImageResource(R.drawable.icon_unfilled_star);
                            five.setImageResource(R.drawable.icon_unfilled_star);
                            finalRating = 3;
                        }
                        break;
                    }
                    case R.id.fourthStar: {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            one.setImageResource(R.drawable.icon_filled_star);
                            two.setImageResource(R.drawable.icon_filled_star);
                            three.setImageResource(R.drawable.icon_filled_star);
                            four.setImageResource(R.drawable.icon_filled_star);
                            five.setImageResource(R.drawable.icon_unfilled_star);

                            finalRating = 4;
                        }
                        break;
                    }
                    case R.id.fifthStar: {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            one.setImageResource(R.drawable.icon_filled_star);
                            two.setImageResource(R.drawable.icon_filled_star);
                            three.setImageResource(R.drawable.icon_filled_star);
                            four.setImageResource(R.drawable.icon_filled_star);
                            five.setImageResource(R.drawable.icon_filled_star);
                            finalRating = 5;
                        }
                        break;

                    }
                }
                return true;
            }
        };

        one.setOnClickListener(ratingClick);
        two.setOnClickListener(ratingClick);
        three.setOnClickListener(ratingClick);
        four.setOnClickListener(ratingClick);
        five.setOnClickListener(ratingClick);
        one.setOnTouchListener(ratingTouch);
        two.setOnTouchListener(ratingTouch);
        three.setOnTouchListener(ratingTouch);
        four.setOnTouchListener(ratingTouch);
        five.setOnTouchListener(ratingTouch);
        final EditText comments = (EditText) findViewById(R.id.commentText);

        Button submit = (Button) findViewById(R.id.submitButton);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                globalData().accessToken
                Map<String, Object> params = new HashMap<String, Object>();
                //  String key="bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1Y2MzYmMzMzcxMmI2YTkwNjBjZmJlYSIsInBob25lTm8iOiI5ODc4NTYxNDA5IiwibmFtZSI6IiIsInJvbGUiOiJDVVNUT01FUiIsImlhdCI6MTQzOTQ0ODAxOH0.w4FEkAjhyin3WIcRnfQh6ykKX4I9QK7k2Qw9_JhsA9Q";
                params.put("ratingStars", Integer.toString(finalRating));
                params.put("comments", comments.getText().toString());
                params.put("bookingId", bookingId);
                globalData().getWebServices(RateActivity.this).sendFeedback(params, globalData().accessToken, new ResponseResolver<String>(RateActivity.this) {
                    @Override
                    public void onSuccess(String s, Response response) {

                    }

                    @Override
                    public void onFailure(RestError error) {

                    }
                });
            }
        });
    }


}

