package com.capitalone.ondemandatm.activities;

import android.app.Dialog;
import android.os.Bundle;

import com.capitalone.ondemandatm.R;

import fr.tvbarthel.lib.blurdialogfragment.BlurDialogFragment;

/**
 * Created by cl-99 on 8/11/2015.
 */
public class ScheduleATM extends BlurDialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(),android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.confirm_request_dialog);
//        dialog.getWindow().setBackgroundDrawable(
//                new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
        return dialog;
    }
}
