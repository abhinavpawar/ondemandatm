package com.capitalone.ondemandatm.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.capitalone.ondemandatm.R;

/**
 * Created by Abhinav on 8/13/15.
 */
public class AssociateProfileActivity extends BaseActivity {
    TextView name;
    TextView phone;
    ImageView profileImage;
    Button callButton;
    Button textButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_associate_profile);
        name = (TextView) findViewById(R.id.nameText);
        phone = (TextView) findViewById(R.id.phoneText);
        profileImage = (ImageView) findViewById(R.id.profileImage);
        callButton = (Button) findViewById(R.id.callButton);
        textButton = (Button) findViewById(R.id.textMessageButton);
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + phone.getText().toString()));
                startActivity(callIntent);
            }
        });
        textButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", phone.getText().toString());
                startActivity(smsIntent);
            }
        });

    }
}
