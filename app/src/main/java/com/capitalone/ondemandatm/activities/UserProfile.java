package com.capitalone.ondemandatm.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.capitalone.ondemandatm.R;
import com.capitalone.ondemandatm.entities.User;
import com.capitalone.ondemandatm.util.CircleTransform;
import com.capitalone.ondemandatm.util.CommonUtil;
import com.capitalone.ondemandatm.util.ImageUtils;
import com.capitalone.ondemandatm.util.PhoneUtil;
import com.capitalone.ondemandatm.util.ProgressTypedFile;
import com.capitalone.ondemandatm.util.ResponseResolver;
import com.capitalone.ondemandatm.util.RestError;
import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit.client.Response;
import retrofit.mime.TypedString;

public class
        UserProfile extends BaseActivity implements ImageUtils.ImageLoadingListener, PhoneUtil.NumberChangedListener {

    TextView textPrimary;
    TextView textSecondary;
    Button done;
    EditText phone;
    EditText name;
    ImageView profilePic;
    SwitchCompat wheelchairToggle;
    boolean firstLogin;
    ImageUtils imageUtil;
    boolean imageUpdated;
    boolean phoneUpdated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        CommonUtil.applyCustomFont(this, (TextInputLayout) findViewById(R.id.nameInputLayout));
        CommonUtil.applyCustomFont(this, (TextInputLayout) findViewById(R.id.phoneInputLayout));
        profilePic = (ImageView) findViewById(R.id.imageView);
        textPrimary = (TextView) findViewById(R.id.textPrimary);
        textSecondary = (TextView) findViewById(R.id.textSecondary);
        done = (Button) findViewById(R.id.done);
        phone = (EditText) findViewById(R.id.phoneNumber);
        name = (EditText) findViewById(R.id.name);
        wheelchairToggle = (SwitchCompat) findViewById(R.id.wheelchair_toggle);

        PhoneUtil.getInstance().setValidationFilter(PhoneUtil.TextType.PhoneNumber, globalData().user.phoneNo, this, phone);
        phone.setText(globalData().user.phoneNo);
        name.setText(globalData().user.name);
        if (globalData().user != null && globalData().user.profilePicURL != null) {
            if (globalData().user.profilePicURL.original != null && !globalData().user.profilePicURL.original.isEmpty()) {
                Picasso.with(this)
                        .load(globalData().user.profilePicURL.original)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .error(R.drawable.avatar_icon_white)
                        .placeholder(R.drawable.avatar_icon_white)
                        .transform(new CircleTransform()).fit()
                        .into(profilePic);

            }
        }
        wheelchairToggle.setChecked(globalData().user.wheelChairAccess);

        firstLogin = getIntent().hasExtra("firstLogin");
        if (firstLogin) {
            textPrimary.setText("Everything look ok?");
            textSecondary.setText("Verify your information and we can get started");
            done.setText("Let's Go!");
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close:
                finish();
                break;

            case R.id.imageView:
                imageUtil = new ImageUtils(this);
                imageUtil.chooseImage();
                break;

            case R.id.done:
                final String phoneNum = phone.getText().toString().replaceAll("[^0-9]", "");
                phoneUpdated = !phoneNum.equals(globalData().user.phoneNo);
                if (firstLogin) {
                    Intent intent = new Intent(this, MapsActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                }
                ProgressTypedFile.Listener listener = new ProgressTypedFile.Listener() {
                    @Override
                    public void onUpdateProgress(int percentage) {
//                        ProgressDialog.updateProgress(percentage);
                    }
                };
                Map<String, Object> params = new HashMap<>();
                if (imageUpdated) {
                    params.put("profilePic", new ProgressTypedFile("image/*", ImageUtils.getTempImageFile(true), listener));
                }
                if (phoneUpdated) {
                    params.put("phoneNo", phoneNum);
                }
//                params.put("wheelChairAccess", new TypedByteArray(null,new Byte(wheelchairToggle.isChecked())));
                params.put("wheelChairAccess", String.valueOf(wheelchairToggle.isChecked()));

                params.put("name", new TypedString(name.getText().toString()));
                globalData().getWebServices(this).updateProfile(globalData().accessToken,
                        params, new ResponseResolver<String>(this) {
                            @Override
                            public void onSuccess(String s, Response response) {
                                Log.d("TAG", s);
                                try {
                                    JSONObject obj = new JSONObject(s);
                                    obj = obj.getJSONObject("data");
                                    globalData().user =
                                            new Gson().fromJson(obj.getJSONObject("userData").toString(), User.class);
                                    if (phoneUpdated) {
                                        Intent intent = new Intent(UserProfile.this, MobileVerification.class);
                                        intent.putExtra("phoneNo", phoneNum);
                                        intent.putExtra("profile_verify", true);
                                        startActivity(intent);
                                    } else {
                                        CommonUtil.showDialog(UserProfile.this, "Success", "Profile updated successfully", false);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(RestError error) {

                            }
                        });
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imageUtil.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        imageUtil = null;
    }

    @Override
    public void onImageLoad(Bitmap bitmap) {
        imageUpdated = true;
        profilePic.setImageBitmap(bitmap);
    }

    @Override
    public void onChanged(boolean isChanged) {
        phoneUpdated = isChanged;
        done.setText(phoneUpdated ? "Verify Number" : (firstLogin ? "Let's Go" : "Done"));
    }
}
