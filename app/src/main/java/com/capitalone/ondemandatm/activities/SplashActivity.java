package com.capitalone.ondemandatm.activities;

import android.os.Bundle;

import com.capitalone.ondemandatm.R;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        if (globalData().accessToken.isEmpty()) {
//            Intent intent = new Intent(this, IntroActivity.class);
//            startActivity(intent);
//            finish();
//        } else {
//            globalData().getWebServices(this).getBookingStatus(globalData().accessToken, new ResponseResolver<String>(this) {
//                @Override
//                public void onSuccess(String s, Response response) {
//                    try {
//                        JSONObject obj = new JSONObject(s);
//                        obj = obj.getJSONObject("data");
//                        globalData().user =
//                                new Gson().fromJson(obj.getJSONObject("userDetails").toString(), User.class);
//                        Intent intent = new Intent(SplashActivity.this, MapsActivity.class);
//                        startActivity(intent);
//                        finish();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                @Override
//                public void onFailure(RestError error) {
//                    Log.d("TAG", error.toString());
//                }
//            });
//        }
        new ScheduleATM().show(getFragmentManager(),null);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
