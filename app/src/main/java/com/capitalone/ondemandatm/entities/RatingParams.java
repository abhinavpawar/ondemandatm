package com.capitalone.ondemandatm.entities;

/**
 * Created by Abhinav on 8/12/15.
 */
public class RatingParams {

        private String bookingId;

        private String ratingStars;

        private String comments;

    public RatingParams(String bookingId, String ratingStars, String comments) {
        this.bookingId = bookingId;
        this.ratingStars = ratingStars;
        this.comments = comments;
    }

    public String getBookingId ()
        {
            return bookingId;
        }

        public void setBookingId (String bookingId)
        {
            this.bookingId = bookingId;
        }

        public String getRatingStars ()
        {
            return ratingStars;
        }

        public void setRatingStars (String ratingStars)
        {
            this.ratingStars = ratingStars;
        }

        public String getComments ()
        {
            return comments;
        }

        public void setComments (String comments)
        {
            this.comments = comments;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [bookingId = "+bookingId+", ratingStars = "+ratingStars+", comments = "+comments+"]";
        }
}



