package com.capitalone.ondemandatm.entities;

/**
 * Created by cl-99 on 7/17/2015.
 */
public class User {

    public String name;
    public String phoneNo;
    public String role;
    public ProfilePicURL profilePicURL;
    public boolean wheelChairAccess;
    public boolean isVerified;
    public boolean isBlocked;
    public boolean isCapitalOne360Customer;
    public boolean isRegistered;
    public String newNumber;

    public class ProfilePicURL {
        public String thumbnail;
        public String original;
    }
}
